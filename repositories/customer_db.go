package repositories

import (
	"itmx/models"

	"gorm.io/gorm"
)

type customerRepositoryDB struct {
	db *gorm.DB
}

func NewCustomerRepositoryDB(db *gorm.DB) customerRepositoryDB {
	return customerRepositoryDB{db: db}
}

func (r customerRepositoryDB) GetById(id int) (*models.Customer, error) {
	customer := models.Customer{}
	result := r.db.First(&customer, id)

	if result.Error != nil {
		return nil, result.Error
	}

	return &customer, nil
}

func (r customerRepositoryDB) DelById(id int) (*models.Customer, error) {
	customer := models.Customer{}
	result := r.db.Delete(&customer, id)

	if result.Error != nil {
		return nil, result.Error
	} else if result.RowsAffected < 1 {
		return nil, gorm.ErrRecordNotFound
	}

	return nil, nil
}

func (r customerRepositoryDB) CreateCustomer(name string, age int) (*models.Customer, error) {
	customer := models.Customer{Name: name, Age: age}
	result := r.db.Create(&customer)

	if result.Error != nil {
		return nil, result.Error
	}

	return &customer, nil
}

func (r customerRepositoryDB) PutCustomer(id int, name string, age int) (*models.Customer, error) {
	customer := models.Customer{}
	result := r.db.First(&customer, id)
	if result.Error != nil {
		return nil, result.Error
	}

	customer = models.Customer{ID: id, Name: name, Age: age}
	result = r.db.Save(&customer)

	if result.Error != nil {
		return nil, result.Error
	}

	return &customer, nil
}
