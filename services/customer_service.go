package services

import (
	"itmx/interfaces"
	"itmx/models"
	"itmx/utils"
)

type customerService struct {
	cr interfaces.CustomerRepository
}

func NewCustomerService(cr interfaces.CustomerRepository) customerService {
	return customerService{cr: cr}
}

func (s customerService) GetCustomer(id int) (*models.CustomerResponse, error) {

	customer, err := s.cr.GetById(id)
	if err != nil {
		return nil, err
	}

	customerResponse := models.CustomerResponse{
		ID:   customer.ID,
		Name: customer.Name,
		Age:  customer.Age,
	}
	return &customerResponse, nil
}

func (s customerService) DelCustomer(id int) (*models.CustomerResponse, error) {
	_, err := s.cr.DelById(id)
	if err != nil {
		return nil, err
	}
	return nil, nil
}

func (s customerService) CreateCustomer(name string, age int) (*models.CustomerResponse, error) {
	if name == "" {
		return nil, utils.ErrNameRequire
	}

	if age <= 0 || age > 120 {
		return nil, utils.ErrAgeRequire
	}

	customer, err := s.cr.CreateCustomer(name, age)
	if err != nil {
		return nil, err
	}

	customerResponse := models.CustomerResponse{
		ID:   customer.ID,
		Name: customer.Name,
		Age:  customer.Age,
	}
	return &customerResponse, nil
}

func (s customerService) PutCustomer(id int, name string, age int) (*models.CustomerResponse, error) {
	if name == "" {
		return nil, utils.ErrNameRequire
	}

	if age <= 0 || age > 120 {
		return nil, utils.ErrAgeRequire
	}

	customer, err := s.cr.PutCustomer(id, name, age)
	if err != nil {
		return nil, err
	}

	customerResponse := models.CustomerResponse{
		ID:   customer.ID,
		Name: customer.Name,
		Age:  customer.Age,
	}
	return &customerResponse, nil
}
