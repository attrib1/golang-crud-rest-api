package services_test

import (
	"fmt"
	mock_repositories "itmx/interfaces/mock"
	"itmx/models"
	"itmx/services"
	"itmx/utils"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestGetCustomer(t *testing.T) {
	ctrl := gomock.NewController(t)
	customerRepo := mock_repositories.NewMockCustomerRepository(ctrl)

	testCase := []struct {
		name    string
		req     int
		want    *models.CustomerResponse
		wantErr bool
		mock    func()
	}{
		{
			name: "success case",
			req:  1,
			want: &models.CustomerResponse{
				ID:   1,
				Name: "Pop",
				Age:  30,
			},
			wantErr: false,
			mock: func() {
				customerRepo.EXPECT().GetById(gomock.Any()).Return(
					&models.Customer{
						ID:   1,
						Name: "Pop",
						Age:  30,
					}, nil)
			},
		},
		{
			name:    "fail case",
			req:     10,
			want:    nil,
			wantErr: true,
			mock: func() {
				customerRepo.EXPECT().GetById(gomock.Any()).Return(
					nil, utils.ErrMock)
			},
		},
	}

	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mock != nil {
				tt.mock()
			}

			service := services.NewCustomerService(customerRepo)

			got, err := service.GetCustomer(tt.req)

			if (err != nil) != tt.wantErr {
				t.Errorf("service.GetCustomer() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !assert.Equal(t, tt.want, got) {
				t.Errorf("service.GetCustomer() = %v, want %v", got, tt.want)
				fmt.Printf("got = %#v, want = %#v\n", got, tt.want)
			}
		})
	}

} // End TestGetCustomer

func TestDelCustomer(t *testing.T) {
	ctrl := gomock.NewController(t)
	customerRepo := mock_repositories.NewMockCustomerRepository(ctrl)

	testCase := []struct {
		name    string
		req     int
		want    *models.CustomerResponse
		wantErr bool
		mock    func()
	}{
		{
			name:    "success case",
			req:     1,
			want:    nil,
			wantErr: false,
			mock: func() {
				customerRepo.EXPECT().DelById(gomock.Any()).Return(
					nil, nil)
			},
		},
		{
			name:    "fail case",
			req:     10,
			want:    nil,
			wantErr: true,
			mock: func() {
				customerRepo.EXPECT().DelById(gomock.Any()).Return(
					nil, utils.ErrMock)
			},
		},
	}

	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mock != nil {
				tt.mock()
			}

			service := services.NewCustomerService(customerRepo)

			got, err := service.DelCustomer(tt.req)

			if (err != nil) != tt.wantErr {
				t.Errorf("service.DelCustomer() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !assert.Equal(t, tt.want, got) {
				t.Errorf("service.DelCustomer() = %v, want %v", got, tt.want)
				fmt.Printf("got = %#v, want = %#v\n", got, tt.want)
			}
		})
	}

} // End TestDelCustomer

func TestCreateCustomer(t *testing.T) {
	ctrl := gomock.NewController(t)
	customerRepo := mock_repositories.NewMockCustomerRepository(ctrl)

	testCase := []struct {
		name     string
		req_name string
		req_age  int
		want     *models.CustomerResponse
		wantErr  bool
		mock     func()
	}{
		{
			name:     "success case",
			req_name: "Pop",
			req_age:  25,
			want: &models.CustomerResponse{
				ID:   2,
				Name: "Pop",
				Age:  25,
			},
			wantErr: false,
			mock: func() {
				customerRepo.EXPECT().CreateCustomer(gomock.Any(), gomock.Any()).Return(
					&models.Customer{
						ID:   2,
						Name: "Pop",
						Age:  25,
					}, nil)
			},
		},
		{
			name:     "fail case",
			req_name: "Pop",
			req_age:  20,
			want:     nil,
			wantErr:  true,
			mock: func() {
				customerRepo.EXPECT().CreateCustomer(gomock.Any(), gomock.Any()).Return(
					nil, utils.ErrMock)
			},
		},
	}

	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mock != nil {
				tt.mock()
			}

			service := services.NewCustomerService(customerRepo)

			got, err := service.CreateCustomer(tt.req_name, tt.req_age)

			if (err != nil) != tt.wantErr {
				t.Errorf("service.CreateCustomer() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !assert.Equal(t, tt.want, got) {
				t.Errorf("service.CreateCustomer() = %v, want %v", got, tt.want)
				fmt.Printf("got = %#v, want = %#v\n", got, tt.want)
			}

		})
	}

	testCases := []struct {
		name     string
		req_name string
		req_age  int
		want     error
		wantErr  bool
	}{
		{
			name:     "err name require",
			req_name: "",
			req_age:  25,
			want:     utils.ErrNameRequire,
			wantErr:  false,
		},
		{
			name:     "err age require",
			req_name: "Pop",
			req_age:  0,
			want:     utils.ErrAgeRequire,
			wantErr:  false,
		},
		{
			name:     "err require",
			req_name: "",
			req_age:  0,
			want:     utils.ErrNameRequire,
			wantErr:  false,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			service := services.NewCustomerService(customerRepo)

			_, err := service.CreateCustomer(tt.req_name, tt.req_age)

			assert.ErrorIs(t, err, tt.want)

		})
	}

} //End TestCreateCustomer

func TestPutCustomer(t *testing.T) {
	ctrl := gomock.NewController(t)
	customerRepo := mock_repositories.NewMockCustomerRepository(ctrl)

	testCase := []struct {
		name     string
		req_name string
		req_age  int
		want     *models.CustomerResponse
		wantErr  bool
		mock     func()
	}{
		{
			name:     "success case",
			req_name: "Pong",
			req_age:  25,
			want: &models.CustomerResponse{
				ID:   2,
				Name: "Pong",
				Age:  25,
			},
			wantErr: false,
			mock: func() {
				customerRepo.EXPECT().PutCustomer(gomock.Any(), gomock.Any(), gomock.Any()).Return(
					&models.Customer{
						ID:   2,
						Name: "Pong",
						Age:  25,
					}, nil)
			},
		},
		{
			name:     "fail case",
			req_name: "Pop",
			req_age:  20,
			want:     nil,
			wantErr:  true,
			mock: func() {
				customerRepo.EXPECT().PutCustomer(gomock.Any(), gomock.Any(), gomock.Any()).Return(
					nil, utils.ErrMock)
			},
		},
	}

	for _, tt := range testCase {
		t.Run(tt.name, func(t *testing.T) {
			if tt.mock != nil {
				tt.mock()
			}

			service := services.NewCustomerService(customerRepo)

			got, err := service.PutCustomer(1, tt.req_name, tt.req_age)

			if (err != nil) != tt.wantErr {
				t.Errorf("service.CreateCustomer() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !assert.Equal(t, tt.want, got) {
				t.Errorf("service.CreateCustomer() = %v, want %v", got, tt.want)
				fmt.Printf("got = %#v, want = %#v\n", got, tt.want)
			}

		})
	}

	testCases := []struct {
		name     string
		req_name string
		req_age  int
		want     error
		wantErr  bool
	}{
		{
			name:     "err name require",
			req_name: "",
			req_age:  25,
			want:     utils.ErrNameRequire,
			wantErr:  false,
		},
		{
			name:     "err age require",
			req_name: "Pop",
			req_age:  0,
			want:     utils.ErrAgeRequire,
			wantErr:  false,
		},
		{
			name:     "err require",
			req_name: "",
			req_age:  0,
			want:     utils.ErrNameRequire,
			wantErr:  false,
		},
	}

	for _, tt := range testCases {
		t.Run(tt.name, func(t *testing.T) {
			service := services.NewCustomerService(customerRepo)

			_, err := service.PutCustomer(1, tt.req_name, tt.req_age)

			assert.ErrorIs(t, err, tt.want)

		})
	}

} //End TestPutCustomer
