### Run
```
go run main.go  
```
### Test
```
 go test itmx/services --cover  
```
### Example
**POST**: /customers
```
curl --location 'http://127.0.0.1:8000/customers' \
--header 'Content-Type: application/json' \
--data '{"name": "Pong","age": 25}'
```
**PUT**: /customers/{id}
```
curl --location --request PUT 'http://127.0.0.1:8000/customers/3' \
--header 'Content-Type: application/json' \
--data '{"name": "Ping Pong", "age": 26}'
```
**DELETE**: /employees/{id}
```
curl --location --request DELETE 'http://127.0.0.1:8000/employees/3'
```
**GET**: /employees/{id}
```
curl --location 'http://127.0.0.1:8000/employees/1'
```