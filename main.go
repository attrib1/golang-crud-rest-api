package main

import (
	handler "itmx/handlers"
	"itmx/models"
	"itmx/repositories"
	"itmx/services"
	_ "net/http"

	"github.com/gofiber/fiber/v2"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

func main() {

	var err error
	db, err = gorm.Open(sqlite.Open("customers.db"), &gorm.Config{})

	if err != nil {
		panic("failed to connect database")
	}

	db.AutoMigrate(&models.Customer{})

	customerRepository := repositories.NewCustomerRepositoryDB(db)
	customerService := services.NewCustomerService(customerRepository)
	customerHandler := handler.NewCustomerHandler(customerService)

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("OK")
	})

	apiCustomers := app.Group("/customers")
	apiCustomers.Post("/", customerHandler.CreateCustomer)
	apiCustomers.Put("/:id", customerHandler.PutCustomer)

	apiEmployees := app.Group("/employees")
	apiEmployees.Get("/:id", customerHandler.GetCustomer)
	apiEmployees.Delete("/:id", customerHandler.DelCustomer)

	app.Listen(":8000")
}
