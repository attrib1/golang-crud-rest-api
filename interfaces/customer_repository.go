package interfaces

import "itmx/models"

type CustomerRepository interface {
	CreateCustomer(string, int) (*models.Customer, error)
	PutCustomer(int, string, int) (*models.Customer, error)
	GetById(int) (*models.Customer, error)
	DelById(int) (*models.Customer, error)
}
