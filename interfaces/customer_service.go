package interfaces

import "itmx/models"

type CustomerService interface {
	CreateCustomer(string, int) (*models.CustomerResponse, error)
	PutCustomer(int, string, int) (*models.CustomerResponse, error)
	GetCustomer(int) (*models.CustomerResponse, error)
	DelCustomer(int) (*models.CustomerResponse, error)
}
