package utils

import "errors"

var (
	ErrNameRequire = errors.New("name is require")
)

var (
	ErrAgeRequire = errors.New("age is require")
)

var (
	ErrMock = errors.New("mock error")
)
