package models

type CreateCustomerHandler struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}
