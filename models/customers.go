package models

type Customer struct {
	ID   int
	Name string `gorm:"not null"`
	Age  int    `gorm:"not null"`
}
