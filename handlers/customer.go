package handlers

import (
	"errors"
	"itmx/interfaces"
	"itmx/models"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type customerHandler struct {
	custSrv interfaces.CustomerService
}

func NewCustomerHandler(custSrv interfaces.CustomerService) customerHandler {
	return customerHandler{custSrv: custSrv}
}

func (h customerHandler) GetCustomer(c *fiber.Ctx) error {
	var paramsId, err = c.ParamsInt("id")

	if err != nil {
		return fiber.ErrBadRequest
	}

	customer, err := h.custSrv.GetCustomer(paramsId)
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{})
	}

	return c.Status(fiber.StatusOK).JSON(customer)
}

func (h customerHandler) DelCustomer(c *fiber.Ctx) error {
	paramsId, err := c.ParamsInt("id")
	if err != nil {
		return fiber.ErrBadRequest
	}

	_, err = h.custSrv.DelCustomer(paramsId)
	if err != nil {
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{})
	}

	return c.Status(fiber.StatusNoContent).JSON(fiber.Map{})
}

func (h customerHandler) CreateCustomer(c *fiber.Ctx) error {
	data := models.CreateCustomerHandler{}
	err := c.BodyParser(&data)
	if err != nil {
		return err
	}

	customer, err := h.custSrv.CreateCustomer(data.Name, data.Age)
	if err != nil {
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{})
	}

	return c.Status(fiber.StatusCreated).JSON(customer)
}

func (h customerHandler) PutCustomer(c *fiber.Ctx) error {
	paramsId, err := c.ParamsInt("id")
	if err != nil {
		return fiber.ErrBadRequest
	}

	data := models.CreateCustomerHandler{}
	err = c.BodyParser(&data)
	if err != nil {
		return err
	}

	customer, err := h.custSrv.PutCustomer(paramsId, data.Name, data.Age)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return c.Status(fiber.StatusNotFound).JSON(fiber.Map{})
		} else {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{})
		}
	}

	return c.Status(fiber.StatusOK).JSON(customer)
}
